/*global chrome*/
// import React, { useEffect, useState } from 'react';
import React, { useEffect } from 'react';
import './App.css';

const App = () => {
  // const [responseFromContent, setResponseFromContent] = useState('');

  useEffect(() => {
    chrome.runtime.sendMessage({
      message: "get_name"
    }, response => {
      if (response.message === 'success') {
        document.querySelector('.hh-name').innerHTML = `Hello ${response.payload}`;
      }
    });
  }, []);

  // const sendTestMessage = () => {
  //   const message = {
  //       message: "Hello from React",
  //   }

  //   const queryInfo = chrome.tabs.QueryInfo = {
  //       active: true,
  //       currentWindow: true
  //   };

  //   /**
  //    * We can't use "chrome.runtime.sendMessage" for sending messages from React.
  //    * For sending messages from React we need to specify which tab to send it to.
  //    */
  //   chrome.tabs && chrome.tabs.query(queryInfo, tabs => {
  //       const currentTabId = tabs[0].id;
  //       /**
  //        * Sends a single message to the content script(s) in the specified tab,
  //        * with an optional callback to run when a response is sent back.
  //        *
  //        * The runtime.onMessage event is fired in each content script running
  //        * in the specified tab for the current extension.
  //        */
  //       chrome.tabs.sendMessage(
  //           currentTabId,
  //           message,
  //           (response) => {
  //               setResponseFromContent(response);
  //           });
  //     });
  // };

  // const sendRemoveMessage = () => {
  //     const message = {
  //         message: "delete logo",
  //     }

  //     const queryInfo = chrome.tabs.QueryInfo = {
  //         active: true,
  //         currentWindow: true
  //     };

  //     chrome.tabs && chrome.tabs.query(queryInfo, tabs => {
  //         const currentTabId = tabs[0].id;
  //         chrome.tabs.sendMessage(
  //             currentTabId,
  //             message,
  //             (response) => {
  //                 setResponseFromContent(response);
  //             });
  //     });
  // };

  return (
    <div className="App">
      <div className="hh-name">
        Hello NAME
      </div>
      {/* <div>
        <button onClick={() => sendTestMessage()}>SEND MESSAGE</button>
        <button onClick={() => sendRemoveMessage()}>Remove logo</button>
        <p>Response from content:</p>
        <p>
          {responseFromContent}
        </p>
      </div> */}
    </div>
  );
}

export default App;
